#!/bin/bash
FIRST_ARGUMENT="$1"

if [ "$FIRST_ARGUMENT" = "replacement-libs" ] ; then
  \cp -rf replacement-libs/leaflet.locatecontrol/L.Control.Locate.js node_modules/leaflet.locatecontrol/src/L.Control.Locate.js
fi