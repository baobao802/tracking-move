export const routeDataMapper = (obj) => {
  return {
    route: obj['Trajet'],
    batchId: obj['ID Lot'],
    driverId: obj['ID Driver'],
    coords: obj['Coordinates'],
  };
};
