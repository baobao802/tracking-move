import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isTruckStarted: false,
};

export const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    setTruckStarted: (state, action) => {
      state.isTruckStarted = action.payload;
    },
  },
});

export const { setTruckStarted } = homeSlice.actions;

export default homeSlice.reducer;
