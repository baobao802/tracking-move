import chroma from "chroma-js";

export const generateColors = (amountTruck, arrColor = []) => {
  let tempColors = arrColor;
  for (let i = 0; i < amountTruck - arrColor.length; i++) {
    const newColor = chroma.random().darken().hex();
    if (!tempColors.includes(newColor)) {
      tempColors = [...tempColors, newColor];
    } else {
      i--;
    }
  }
  return tempColors;
};
